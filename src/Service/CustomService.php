<?php

namespace Drupal\custom_module\Service;

use Drupal\Core\Session\AccountProxy;

/**
 * Class CustomService.
 */
class CustomService {

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountProxy $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * Get current user account name.
   */
  public function getAccountName() {
    return $this->currentUser->getAccountName();
  }

  /**
   * Return foo !
   */
  public function doFoo() {
    return 'foo';
  }

}
