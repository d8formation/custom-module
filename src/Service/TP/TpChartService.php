<?php

namespace Drupal\custom_module\Service\TP;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class TpChartService.
 */
class TpChartService {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the user's repartion, by level, for one sport.
   */
  public function getLevelRepartitionBySport(string $pole) {
    $results = $this->entityTypeManager->getStorage('user')->getAggregateQuery()
      ->aggregate('field_niveau', 'COUNT')
      ->condition('field_sport', $pole,)
      ->groupBy('field_niveau')
      ->execute();
    return $results;
  }

  /**
   * Get the user's repartion, by level, for one sport.
   */
  public function getLevelRepartitionByLevel(string $level) {
    $results = $this->entityTypeManager->getStorage('user')->getAggregateQuery()
      ->aggregate('field_sport', 'COUNT')
      ->condition('field_niveau', $level,)
      ->groupBy('field_sport')
      ->sort('field_sport')
      ->execute();
    return $results;
  }

  /**
   * Prepare data for renderinfg a pie chart.
   */
  public function preparePieChart(array $slices) {
    if (empty($slices)) {
      return;
    }
    $datas = [];
    foreach ($slices as $slice) {
      $level = $this->entityTypeManager->getStorage('taxonomy_term')->load($slice['field_niveau_target_id']);
      $datas[] = [
        'level' => $level->getName(),
        'count' => $slice['field_niveau_count'],
      ];
    }
    return $datas;
  }

  /**
   * Prepare data for renderinfg a stack chart.
   */
  public function prepareStackChart(array $slices, string $name) {
    if (empty($slices)) {
      return;
    }
    $datas = [];
    foreach ($slices as $slice) {
      $sport = $this->entityTypeManager->getStorage('taxonomy_term')->load($slice['field_sport_target_id']);
      $datas[$name][$sport->getName()] = $slice['field_sport_count'];
    }
    return $datas;
  }

}
