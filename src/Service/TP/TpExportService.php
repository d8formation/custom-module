<?php

namespace Drupal\custom_module\Service\TP;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TpExportService.
 */
class TpExportService {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get term name.
   */
  public function getTermName(string $tid) {
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($tid);
    if (!is_null($term)) {
      return strtolower($term->getName());
    }
    return '';
  }

  /**
   * Get all members.
   * Return csv data.
   */
  public function getAllMembers() {
    $uids = $this->getMembers();
    return $this->getCsvData($uids);
  }

  /**
   * Get all members, by pole, registred since first day of the current year.
   * Return csv data.
   */
  public function getAllMembersByPole(string $tid) {
    $uids = $this->getMembers($tid);
    return $this->getCsvData($uids);
  }

  /**
   * Get all members, by pole, registred since first day of the current year.
   * Return csv data.
   */
  public function getMembersSelectedByForm(string $tid, string $start, string $stop) {
    $uids = $this->getMembers($tid, $start, $stop);
    return $this->getCsvData($uids);
  }

  /**
   * Get the members uids.
   */
  private function getMembers(string $tid = NULL, string $start = NULL, string $stop = NULL) {

    $query = $this->entityTypeManager->getStorage('user')->getQuery();

    if ($tid != '') {
      $query->condition('field_sport', $tid, '=');
    }

    if (($start != '') && ($stop != '')) {
      $query->condition('created', [$start, $stop], 'BETWEEN');
    }
    if (($start != '') && ($stop == '')) {
      $query->condition('created', [$start, $start + 86400], 'BETWEEN');
    }
    if (($start == '') && ($stop != '')) {
      $query->condition('created', $stop, '<');
    }
    return $query->execute();
  }

  /**
   * Return data in csv.
   */
  public function returnCsv($output, $name, $now) {
    $response = new Response();
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename=export_' . $name . '_' . $now . '.csv');
    $response->setContent($output);
    return $response;
  }

  /**
   * Get data in csv.
   */
  private function getCsvData(array $uids) {
    $handle = fopen('php://temp', 'w+');
    $header = [
      'Nom d\'utilisateur',
      'Email',
      'Enregistré le',
    ];
    fputcsv($handle, $header);

    $users = $this->entityTypeManager->getStorage('user')->loadMultiple($uids);
    foreach ($users as $user) {
      if ($user->id() != 0) {
        $csv = [
          $user->getAccountName(),
          $user->getEmail(),
          date('d/m/Y', $user->getCreatedTime()),
        ];
        fputcsv($handle, $csv);
      }
    }
    rewind($handle);
    $csv_data = stream_get_contents($handle);
    fclose($handle);
    return $csv_data;
  }

}
