<?php

namespace Drupal\custom_module\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class CustomEvent.
 */
class CustomEvent extends Event {

  // All constant links to the EventSubscriber class.
  // @see custom_module.services.yml
  const CUSTOM_HANDLER = 'custom_module.custom_handler';

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity() {
    return $this->entity;
  }

}
