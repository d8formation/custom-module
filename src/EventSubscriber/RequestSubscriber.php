<?php

namespace Drupal\custom_module\EventSubscriber;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\State\StateInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class RequestSubscriber.
 */
class RequestSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $stateInterface;

  /**
   * {@inheritdoc}
   */
  public function __construct(StateInterface $state_interface) {
    $this->stateInterface = $state_interface;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection'];
    return $events;
  }

  /**
   * Callback fonction.
   */
  public function checkForRedirection(GetResponseEvent $event) {
    $enabled = $this->stateInterface->get('system.maintenance_mode');
    if ($enabled === 1) {
      $event->setResponse(new TrustedRedirectResponse('http://qwant.fr/'));
    }
  }

}
