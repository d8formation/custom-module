<?php

namespace Drupal\custom_module\EventSubscriber;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class RouteSubscriber.
 */
class RouteSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::ALTER] = 'alterRoute';
    return $events;
  }

  /**
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   */
  public function alterRoute(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();

    if ($route = $collection->get('user.login')) {
      $route->setPath('/se-connecter');
    }
    if ($route = $collection->get('user.pass')) {
      $route->setPath('/nouveau-mot-de-passe');
    }
  }

}
