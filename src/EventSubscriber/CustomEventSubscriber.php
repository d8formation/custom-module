<?php

namespace Drupal\custom_module\EventSubscriber;

use Drupal\custom_module\Event\CustomEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CustomEventSubscriber.
 */
class CustomEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[CustomEvent::CUSTOM_HANDLER][] = ['customCallback'];
    return $events;
  }

  /**
   * @param \Drupal\custom_module\Event\CustomEvent $event
   */
  public function customCallback(CustomEvent $event) {
    $entity = $event->getEntity();
    \Drupal::logger('Event Entity')->notice('Update @type: @title. Created by: @owner', [
      '@type' => $entity->getType(),
      '@title' => $entity->label(),
      '@owner' => $entity->getOwner()->getDisplayName(),
    ]);
  }

}
