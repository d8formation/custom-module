<?php

namespace Drupal\custom_module\EventSubscriber\TP;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\StringTranslation\TranslationManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Class RequestSubscriber.
 */
class TpRedirectionSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $messenger;

  /**
   * @var \rupal\Core\StringTranslation\TranslationManager
   */
  protected $translationManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(Messenger $messenger, TranslationManager $translation_manager) {
    $this->messenger = $messenger;
    $this->translationManager = $translation_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = ['onException'];
    return $events;
  }

  /**
   * Callback fonction.
   */
  public function onException(GetResponseEvent $event) {
    $response = $event->getRequest();
    $status = $response->attributes->get('exception')->getStatusCode();

    switch ($status) {
      case 404 :
        $this->messenger->addMessage($this->translationManager->translate('yop'));
        $event->setResponse(new RedirectResponse(Url::fromRoute('<front>')->toString()));
        break;
    }
  }

}
