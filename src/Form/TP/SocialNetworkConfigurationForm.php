<?php

namespace Drupal\custom_module\Form\TP;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure globale settings.
 */
class SocialNetworkConfigurationForm extends ConfigFormBase {

  /**
   * @var string
   */
  const SETTINGS = 'custom_module.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_module_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['social_network'] = [
      '#type' => 'fieldset',
      '#title' => 'Réseaux sociaux',
    ];
    $form['social_network']['facebook'] = [
      '#type' => 'url',
      '#title' => $this->t('Facebook'),
      '#default_value' => $config->get('facebook'),
    ];
    $form['social_network']['twitter'] = [
      '#type' => 'url',
      '#title' => $this->t('Twitter'),
      '#default_value' => $config->get('twitter'),
    ];
    $form['social_network']['instagram'] = [
      '#type' => 'url',
      '#title' => $this->t('Instagram'),
      '#default_value' => $config->get('instagram'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('facebook', $form_state->getValue('facebook'))
      ->set('twitter', $form_state->getValue('twitter'))
      ->set('instagram', $form_state->getValue('instagram'))
      ->save();

    parent::submitForm($form, $form_state);

  }
}
