<?php

namespace Drupal\custom_module\Form\TP;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NewsletterStepTwoForm.
 */
class NewsletterSubscribeForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;


  /**
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
    Connection $database,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('custom_module');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('user.private_tempstore'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'newsletter_subscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, int $sport = NULL) {

    // TP 3 Formulaire - niveau 2 : gestion de la redirection
    // if (!$this->currentUser()->isAnonymous()) {
    //   $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
    //   $field_newsletter = $account->get('field_newsletter')->getValue();
    //   if (!empty($field_newsletter)) {
    //     $field_newsletter = reset($field_newsletter);
    //     if ($field_newsletter['value']) {
    //       $this->messenger()->addStatus($this->t('You are already registred'));
    //       return $this->redirect('entity.user.edit_form', ['user' => $this->currentUser()->id()]);
    //     }
    //   }
    // }
    //

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#required' =>TRUE,
    ];

    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
      '#required' =>TRUE,
    ];

    if (!$this->currentUser()->isAnonymous()) {
      // Adaptation du code pour le TP block niveau 2
      $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
      $form['name']['#default_value'] = $account->getAccountName();
      $form['mail']['#default_value'] = $account->getEmail();
    }

    $form['sport'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose an activity'),
      '#required' => TRUE,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'sport',
        ],
      ],
    ];

    if (!is_null($sport)) {
      $form['sport']['#default_value'] = $this->entityTypeManager->getStorage('taxonomy_term')->load($sport);
      $form['sport']['#disabled'] = TRUE;
    }

    // $options = [];
    // $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('sport');
    // if (!empty($terms)) {
    //   foreach ($terms as $term) {
    //     $term_name = $term->name;
    //     $options[$term->tid] = $term_name;
    //   }
    // }
    //
    // $form['sport2'] = [
    //   '#type' => 'select',
    //   '#title' => $this->t('Choose an activity'),
    //   '#required' => TRUE,
    //   '#options' => $options,
    //   '#empty_option' => $this->t('Select a sport'),
    // ];

    $form['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your postal code'),
      '#maxlength' => 5,
    ];

    $form['locality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your locality'),
      '#states' => [
        'invisible' => [
          ':input[name="postal_code"]' => ['filled' => FALSE],
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // TP 5 Database : vérification de la préexistence du mail.
    $mail = $form_state->getValue('mail');
    $query = $this->database->select('newsletters_subscription', 'nl')
      ->condition('nl.email', $mail);
    $result = $query->countQuery()->execute()->fetchField();
    if ($result != 0) {
      $form_state->setErrorByName('mail', $this->t('This email is already registred'));
    }

    $postal_code = $form_state->getValue('postal_code');

    if ($postal_code != '') {
      if (strlen($postal_code) < 5) {
        $form_state->setErrorByName('postal_code', $this->t('Enter a valid postal code !'));
      }
      if (!preg_match('/^[0-9]*$/', $postal_code)) {
        $form_state->setErrorByName('postal_code', $this->t('Only number are allowed !'));
      }

      $locality = $form_state->getValue('locality');

      if ($locality == '') {
        $form_state->setErrorByName('locality', $this->t('You must provide your locality'));
      }
      if (preg_match('/\d+/', $locality)) {
        $form_state->setErrorByName('locality', $this->t('Only character are allowed !'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // récupération des données du formulaire
    $values = $form_state->getValues();

    // TP 3 Formulaire - niveau 1 : message et redirection
    $this->messenger()->addStatus($this->t('Your subscription have been submitted'));
    $term_sport = $this->entityTypeManager->getStorage('taxonomy_term')->load($values['sport']);
    $parameters = [
      'name' => $values['name'],
      'mail' => $values['mail'],
      'sport' => $term_sport->getName(),
    ];
    // Redirection with $_GET data
    $form_state->setRedirect('custom_module.newsletter_form_redirection', $parameters);
    //
    // Redirection using tempStore.
    // $this->store->set('newsletters_subscription_form_data', $parameters);
    // $form_state->setRedirect('custom_module.newsletter_form_redirection_ts');


    // TP 3 Formulaire - niveau 2 : gestion de l'utilisateur courant
    if (!$this->currentUser()->isAnonymous()) {
      $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
      $account->set('field_newsletter', ['value' => 1]);
      $account->save();
    }
    // TP 5 Database - niveau 2 : Enregistrement des déonnées en base
    else {
      $fields = [
        'contact_name' => $values['name'],
        'email' => $values['mail'],
        'postal_code' => $values['postal_code'],
        'locality' => $values['locality'],
        'subscription' => 1,
        'last_update' => time(),
      ];

      $this->database->insert('newsletters_subscription')
        ->fields($fields)
        ->execute();
    }
  }

}
