<?php

namespace Drupal\custom_module\Form\TP\NewsletterForm;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class NewsletterStepTwoForm.
 */
class NewsletterStepTwoForm extends NewsletterBaseForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'newsletter_form_two';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your postal code'),
      '#default_value' => $this->store->get('postal_code') ? $this->store->get('postal_code') : '',
    ];

    $form['location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your location'),
      '#default_value' => $this->store->get('location') ? $this->store->get('location') : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->store->set('postal_code', $form_state->getValue('postal_code'));
    $this->store->set('location', $form_state->getValue('location'));

    $fields = [
      'contact_name' => $this->store->get('name'),
      'locality' => $this->store->get('location'),
      'postal_code' => $this->store->get('postal_code'),
      'email' => $this->store->get('email'),
      'subscription' => 1,
      'last_update' => time(),
    ];

    $triggering = $form_state->getTriggeringElement();
    $action = $triggering['#id'];
    switch ($action) {
      case 'edit-submit':
        parent::saveData($fields);
        $form_state->setRedirect('custom_module.newsletter_redirection');
        break;

      case 'edit-reverse':
        $form_state->setRedirect('custom_module.newsletter_one');
        break;

      default:
        $form_state->setRedirect('custom_module.newsletter_one');
    }

  }

}
