<?php

namespace Drupal\custom_module\Form\TP\NewsletterForm;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class NewsletterStepOneForm.
 */
class NewsletterStepOneForm extends NewsletterBaseForm {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'newsletter_form_one';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your name'),
      '#default_value' => $this->store->get('name') ? $this->store->get('name') : '',
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Your email address'),
      '#default_value' => $this->store->get('email') ? $this->store->get('email') : '',
    ];

    $form['actions']['submit']['#value'] = $this->t('Next');
    $form['actions']['reverse']['#access'] = FALSE;
    return $form;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $mail = $form_state->getValue('email');
    $result = parent::validateEmail($mail);

    if ($result != 0) {
      $form_state->setErrorByName('email', $this->t('This email is already registred'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->store->set('name', $form_state->getValue('name'));
    $this->store->set('email', $form_state->getValue('email'));
    $form_state->setRedirect('custom_module.newsletter_two');
  }

}
