<?php

namespace Drupal\custom_module\Form\TP\NewsletterForm;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NewsletterBaseForm.
 */
abstract class NewsletterBaseForm extends FormBase {

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory,
    SessionManagerInterface $session_manager,
  Connection $database) {
    $this->sessionManager = $session_manager;
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('custom_module_newsletter_form');
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('session_manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Start a manual session for anonymous users.
    if ($this->currentUser()->isAnonymous() && !isset($_SESSION['multistep_form_holds_session'])) {
      $_SESSION['multistep_form_holds_session'] = TRUE;
      $this->sessionManager->start();
    }

    $form = [];

    $form['actions']['#type'] = 'actions';

    $form['actions']['reverse'] = [
      '#type' => 'submit',
      '#value' => $this->t('Return'),
      '#weight' => 0,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * Saves the data from the multistep form.
   */
  protected function validateEmail(string $mail) {

    $query = $this->database->select('newsletters_subscription', 'nl')
      ->condition('nl.email', $mail);
    $result = $query->countQuery()->execute()->fetchField();
    return $result;
  }

  /**
   * Saves the data from the multistep form.
   */
  protected function saveData($fields) {

    $this->database->insert('newsletters_subscription')
      ->fields($fields)
      ->execute();

    // Logic for saving data goes here...
    $this->deleteStore();
    $this->messenger()->addStatus($this->t('The form has been saved.'));
  }

  /**
   * Helper method that removes all the keys from the store collection used for
   * the multistep form.
   */
  protected function deleteStore() {
    $keys = ['name', 'email', 'postal_code', 'location'];
    foreach ($keys as $key) {
      $this->store->delete($key);
    }
  }

}
