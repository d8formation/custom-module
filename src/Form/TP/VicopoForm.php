<?php

namespace Drupal\custom_module\Form\TP;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class VicopoForm.
 */
class VicopoForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vicopo_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postal code'),
      '#description' => $this->t('Enter your postal code'),
      '#attributes' => [
        'id' => ['code'],
      ],
    ];

    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#description' => $this->t('Choose your city'),
      '#disabled' => TRUE,
      '#attributes' => [
        'id' => ['city'],
      ],
    ];

    $form['output'] = [
      '#markup' => '<div id="output"></div>',
    ];

    // Attach librairie.
    $form['#attached']['library'][] = 'custom_module/vicopo';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
