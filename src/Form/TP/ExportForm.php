<?php

namespace Drupal\custom_module\Form\TP;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NewsletterStepTwoForm.
 */
class ExportForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entiyTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entiyTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_manage_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $pole = NULL) {

    $form['start'] = [
      '#type' => 'date',
      '#description' => $this->t('Start time'),
    ];

    $form['stop'] = [
      '#type' => 'date',
      '#description' => $this->t('Stop time'),
    ];

    $form['sport'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose an activity'),
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'sport',
        ],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $start = $form_state->getValue('start');
    $stop = $form_state->getValue('stop');

    if ($stop != '') {
      if ($stop < $start) {
        $form_state->setErrorByName('stop', $this->t('The end date must be greater than the start date'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $parameters = [
      'start' => ($values['start'] != '') ? strtotime($values['start']) : NULL,
      'stop' => ($values['stop'] != '') ? strtotime($values['stop']) : NULL,
      'sport' => $values['sport'],
    ];
    $form_state->setRedirect('custom_module.export_member_form_selection', $parameters);
  }

}
