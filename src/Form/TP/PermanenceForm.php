<?php

namespace Drupal\custom_module\Form\TP;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NewsletterStepTwoForm.
 */
class PermanenceForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entiyTypeManager;

  /**
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
    LanguageManager $language_manager,
  PrivateTempStoreFactory $temp_store_factory) {
    $this->entiyTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->tempStoreFactory = $temp_store_factory;
    $this->store = $this->tempStoreFactory->get('custom_module');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('user.private_tempstore'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'permanence_manage_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, int $sport = NULL) {

    // Pour gérer la redirection vers le formulaire back-office.
    //  if (in_array('administrator', $this->currentUser()->getRoles())) {
    // return $this->redirect('node.add', ['node_type' => 'permanence']);
    // }.
    $form['start'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start of the permanence'),
      '#required' => TRUE,
    ];

    $form['stop'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End of the permanence'),
      '#required' => TRUE,
    ];

    $form['sport'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose an activity'),
      '#required' => TRUE,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'sport',
        ],
      ],
    ];

    // Pour le TP block, exercice 3.
    if (!is_null($sport)) {
      $form['sport']['#default_value'] = $this->entiyTypeManager->getStorage('taxonomy_term')->load($sport);
      $form['sport']['#disabled'] = TRUE;
    }

    $form['post_comment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Leave a message'),
    ];

    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => 'Commentaire',
      '#description' => $this->t('Enter your message'),
      '#attributes' => [
        'id' => ['post-comment'],
        'onkeyup' => 'countChar(this)',
      ],
      '#states' => [
        'invisible' => [
          ':input[name="post_comment"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['countChar'] = [
      '#markup' => '<div id="countChar"></div>',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    $form['#attached']['library'][] = 'custom_module/count_char';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $now = new DrupalDateTime();
    $start = $form_state->getValue('start');
    $stop = $form_state->getValue('stop');

    if ($start < $now) {
      $form_state->setErrorByName('start', $this->t('Back to the future ?'));
    }
    if ($stop < $start) {
      $form_state->setErrorByName('stop', $this->t('This is an temporal rift !!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $term_sport = $this->entiyTypeManager->getStorage('taxonomy_term')->load($values['sport']);

    $parameters = [
      'start' => $values['start']->format('d/m/Y\, \à h:i', ['timezone' => $this->currentUser()->getTimezone()]),
      'stop' => $values['stop']->format('d/m/Y\, \à h:i', ['timezone' => $this->currentUser()->getTimezone()]),
      'sport' => $term_sport->getName(),
    ];

    $fields = [
      'type' => 'permanence',
      'language' => $this->languageManager->getCurrentLanguage()->getId(),
      'uid' => $this->currentUser()->id(),
      'revision_uid' => $this->currentUser()->id(),
      'title' => 'Permanance de  ' . $this->currentUser()->getAccountName(),
      'field_adherent' => $this->currentUser()->id(),
      'field_presence' => [
        'value' => $values['start']->format('Y-m-d\Th:i:s', ['timezone' => 'UTC']),
        'end_value' => $values['stop']->format('Y-m-d\Th:i:s', ['timezone' => 'UTC']),
      ],
      'field_sport' => $values['sport'],
      'field_comment' => $values['comment'],
    ];

    $node = $this->entiyTypeManager->getStorage('node')->create($fields);
    $node->save();

    $this->messenger()->addStatus($this->t('Your permanance have been submitted'));

    // Redirection with $_GET data
    // $form_state->setRedirect('custom_module.redirection_permanence', $parameters);
    //
    // Redirection using tempStore.
    $this->store->set('permanence_form_data', $parameters);
    $form_state->setRedirect('custom_module.redirection_permanence_tempstore');
  }

}
