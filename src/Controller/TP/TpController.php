<?php

namespace Drupal\custom_module\Controller\TP;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TpController.
 */
class TpController extends ControllerBase {

  /**
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(DateFormatter $date_formatter) {
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('date.formatter'),
    );
  }

  /**
   * TP Controller - level 1.
   */
  public function renderLv1() {

    // On est obligé de charger l'entité complète pour avoir accès à toutes les
    // informations nécessaires -> date de création.
    $user = $this->entityTypeManager()->getStorage('user')->load($this->currentUser()->id());

    $last_connexion = $this->currentUser()->getLastAccessedTime() != 0 ?
      date('d/m/Y', $this->currentUser()->getLastAccessedTime()) : $this->t('never');

    $field_sport = $user->get('field_sport')->getValue();
    $field_sport = reset($field_sport);
    $sport_id = $field_sport['target_id'];

    $field_niveau = $user->get('field_niveau')->getValue();
    $field_niveau = reset($field_niveau);
    $niveau_id = $field_niveau['target_id'];

    $sport_term = $this->entityTypeManager()->getStorage('taxonomy_term')->load($sport_id);
    $sport = '';
    if (!is_null($sport_term)) {
      $sport = $sport_term->getName();
    }

    $level_term = $this->entityTypeManager()->getStorage('taxonomy_term')->load($niveau_id);
    $level = '';
    if (!is_null($level_term)) {
      $level = $level_term->getName();
    }

    $list = [
      $this->t('Username :') . $this->currentUser()->getAccountName(),
      $this->t('Email :') . $this->currentUser()->getEmail(),
      $this->t('Created :') . date('d/m/Y', $user->getCreatedTime()),
      $this->t('Last connexion :') . $last_connexion,
      $sport,
      $level,
    ];

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $list,
      '#prefix' => '<h1>' . $this->t('About me') . '</h1>',
    ];
  }

  /**
   * TP Controller - level 2.
   */
  public function renderLv2(User $user) {

    $header = [
      $this->t('Username'),
      $this->t('Email'),
      $this->t('Created since'),
      $this->t('Last connexion'),
      $this->t('Sport'),
      $this->t('Level'),
    ];

    $field_sport = $user->get('field_sport')->getValue();
    $field_sport = reset($field_sport);
    $sport_id = $field_sport['target_id'];

    $field_niveau = $user->get('field_niveau')->getValue();
    $field_niveau = reset($field_niveau);
    $niveau_id = $field_niveau['target_id'];

    $sport_term = $this->entityTypeManager()->getStorage('taxonomy_term')->load($sport_id);
    $sport = '';
    if (!is_null($sport_term)) {
      $sport = $sport_term->getName();
    }

    $level_term = $this->entityTypeManager()->getStorage('taxonomy_term')->load($niveau_id);
    $level = '';
    if (!is_null($level_term)) {
      $level = $level_term->getName();
    }

    $output[] = [
      $user->getAccountName(),
      $user->getEmail(),
      $this->dateFormatter->formatTimeDiffSince($user->getCreatedTime()),
      $user->getLastAccessedTime() != 0 ?
      date('d/m/Y', $user->getLastAccessedTime()) : $this->t('never'),
      $sport,
      $level,
    ];

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
      '#prefix' => '<h1>' . $this->t('About') . $user->getAccountName() . '</h1>',
    ];
  }

  /**
   * TP Controller - level 3.
   */
  public function renderLv3(User $user) {

    $header = [
      $this->t('Username'),
      $this->t('Email'),
      $this->t('Created since'),
      $this->t('Last connexion'),
      $this->t('Sport'),
      $this->t('Level'),
    ];

    $field_sport = $user->get('field_sport')->getValue();
    $field_sport = reset($field_sport);
    $sport_id = $field_sport['target_id'];

    $field_niveau = $user->get('field_niveau')->getValue();
    $field_niveau = reset($field_niveau);
    $niveau_id = $field_niveau['target_id'];

    $sport_term = $this->entityTypeManager()->getStorage('taxonomy_term')->load($sport_id);
    $sport = '';
    if (!is_null($sport_term)) {
      $sport = $sport_term->getName();
    }

    $level_term = $this->entityTypeManager()->getStorage('taxonomy_term')->load($niveau_id);
    $level = '';
    if (!is_null($level_term)) {
      $level = $level_term->getName();
    }

    $output[] = [
      $user->getAccountName(),
      $user->getEmail(),
      $this->dateFormatter->formatTimeDiffSince($user->getCreatedTime()),
      $user->getLastAccessedTime() != 0 ?
      date('d/m/Y', $user->getLastAccessedTime()) : $this->t('never'),
      $sport,
      $level,
    ];

    $this->messenger()->addWarning($this->t('Confidentials informations'));

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
    ];
  }

  /**
   * TP Controller - level 3.
   */
  public function titleCallback(User $user) {
    return sprintf('About %s', $user->getAccountName());
  }

}
