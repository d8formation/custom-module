<?php

namespace Drupal\custom_module\Controller\TP;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Driver\mysql\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TpDatabaseController.
 */
class TpDatabaseController extends ControllerBase {

  /**
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('database'),
    );
  }

  /**
   * TP database - level 1.
   */
  public function simpleQuery() {

    // Nombre total de users enregistrés -> count sur la table.
    $query = $this->database->select('users', 'u');
    $result_table = $query->countQuery()->execute()->fetchField();

    // Nombre total de users enregistrés -> avec un entities query.
    $uids = $this->entityTypeManager()->getStorage('user')->getQuery()
      ->execute();
    $result_entities = count($uids);

    // Nombre de permanence pour un user donné.
    $nids = $this->entityTypeManager()->getStorage('node')->getQuery()
      ->condition('type', 'permanence', '')
      ->condition('field_adherent', $this->currentUser()->id())
      ->execute();
    $result_permanence = count($nids);

    // Nombre de permanence pour un pole donné.
    $nids = $this->entityTypeManager()->getStorage('node')->getQuery()
      ->condition('type', 'permanence', '')
      ->condition('field_sport', 10)
      ->execute();
    $result_pole = count($nids);

    $list = [
      'Nombre utilisateur dans la table: ' . $result_table,
      'Nombre utilisateur en entité : ' . $result_entities,
      'Nombre de permanence pour l\'utilisateur courant : ' . $result_permanence,
      'Nombre de permanence pour le sport $tid = 10 : ' . $result_pole,
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $list,
    ];
  }

  /**
   * TP database - level 3.
   */
  public function aggregateQuery() {

    $results = $this->entityTypeManager()->getStorage('node')->getAggregateQuery()
      ->aggregate('nid', 'COUNT')
      ->condition('type', 'permanence')
      ->groupBy('uid')
      ->execute();

    $header = [
      'Nom d\'utilisateurs',
      'Nombre de permaneces',
    ];

    foreach ($results as $record) {
      $user = $this->entityTypeManager()->getStorage('user')->load($record['uid']);
      $output[] = [
        $user->getAccountName(),
        $record['nid_count'],
      ];
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $output,
    ];
  }

}
