<?php

namespace Drupal\custom_module\Controller\TP;

use Drupal\Core\Controller\ControllerBase;
use Drupal\custom_module\Service\TP\TpChartService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TpServiceChartController.
 */
class TpServiceChartController extends ControllerBase {

  /**
   * @var \Drupal\custom_module\Service\TP\TpChartService
   */
  protected $chartService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
       $container->get('custom_module.chart_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(TpChartService $chart_service) {
    $this->chartService = $chart_service;
  }

  /**
   * TP Service : Level 3.
   */
  public function render() {

    $levels = $this->entityTypeManager()->getStorage('taxonomy_term')->loadTree('niva');

    foreach ($levels as $level) {
      $repartition = $this->chartService->getLevelRepartitionByLevel($level->tid);
      $datas = $this->chartService->prepareStackChart($repartition, $level->name);
      $stacks[] = $datas;
    }

    // Manage theme & librairies.
    $build = [
      '#theme' => 'block_chart',
      '#title' => $this->t('Level repartition'),
    ];
    $build['#attached']['library'][] = 'custom_module/highchart';
    $build['#attached']['library'][] = 'custom_module/stackchart';
    // Manage data for js file.
    $build['#attached']['drupalSettings']['datas'] = $stacks;

    return $build;
  }

}
