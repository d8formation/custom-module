<?php

namespace Drupal\custom_module\Controller\TP;

use Drupal\Core\Controller\ControllerBase;
use Drupal\custom_module\Service\TP\TpExportService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class TpExportController.
 */
class TpExportController extends ControllerBase {

  /**
   * @var \Drupal\custom_module\Service\TpExportService
   */
  protected $exportService;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('custom_module.export_service'),
      $container->get('request_stack'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(TpExportService $exportService, RequestStack $request_manager) {
    $this->exportService = $exportService;
    $this->requestManager = $request_manager;
  }

  /**
   * TP Service : Level 1.
   */
  public function exportAllMembers() {
    $now = time();
    $output = $this->exportService->getAllMembers();
    return $this->exportService->returnCsv($output, 'all_members', date("Y-m-d", $now));
  }

  /**
   * TP Service : Level 1.
   */
  public function exportAllMembersByPole(string $tid) {
    $now = time();
    $term_name = $this->exportService->getTermName($tid);
    $name = 'all_members_' . $term_name;
    $output = $this->exportService->getAllMembersByPole($tid);
    return $this->exportService->returnCsv($output, $name, date("Y-m-d", $now));
  }

  /**
   * TP Service : Level 2.
   */
  public function exportMembers() {

    $params = $this->requestManager->getCurrentRequest()->query->all();

    $start = array_key_exists('start', $params) ? $params['start'] : NULL;
    $stop  = array_key_exists('stop', $params) ? $params['stop'] : NULL;
    $sport = array_key_exists('sport', $params) ? $params['sport'] : NULL;

    $term_name = ($sport != '') ? $this->exportService->getTermName($sport) : '';
    $name = 'members_' . $term_name;
    $output = $this->exportService->getMembersSelectedByForm($sport, $start, $stop);
    return $this->exportService->returnCsv($output, $name, date("Y-m-d", time()));
  }

}
