<?php

namespace Drupal\custom_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class CustomController.
 */
class CustomController extends ControllerBase {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestManager;

  /**
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_manager, Connection $database) {
    $this->requestManager = $request_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('request_stack'),
        $container->get('database'),
    );
  }

  /**
   * Simple controller render a markup.
   */
  public function render() {
    return [
      '#markup' => $this->t('Hello you'),
    ];
  }

  /**
   * Controller can use argument.
   */
  public function renderArgument($foo, $bar) {
    return [
      '#markup' => '<p>Args $foo=' . $foo . ' $bar=' . $bar . '</p>',
    ];
  }

  /**
   * Controller can access to the entire entity.
   */
  public function renderEntity(User $user) {
    return [
      '#markup' => '<p>' . $this->t('Hello you') . ' ' . $user->getAccountName() . '</p>',
    ];
  }

  /**
   * You can acces to the query params.
   */
  public function renderFormResult() {

    $params = $this->requestManager->getCurrentRequest()->query->all();
    $input = array_key_exists('input', $params) ? $params['input'] : '';

    $message = '<p>' . $this->t('My redirection page') . '</p>';

    if ($input != '') {
      $message .= '<p>' . $this->t('My input :') . $input . '</p>';
    }
    return [
      '#markup' => $message,
    ];
  }

  /**
   * Database example.
   */
  public function readDatabase() {

    // Requete faite directement sur les tables.
    $query = $this->database->select('users', 'u')
      ->fields('u', ['uid', 'uuid']);
    $result = $query->execute();

    dump('Traitement des reponse dans une boucle');
    foreach ($result as $record) {
      dump($record);
    }

    dump('fetchField');
    dump($query->execute()->fetchField());
    dump('fetchAll');
    dump($query->execute()->fetchAll());
    dump('fetchCol');
    dump($query->execute()->fetchCol());
    dump('fetchAssoc');
    dump($query->execute()->fetchAssoc());
    dump('fetchAllAssoc');
    dump($query->execute()->fetchAllAssoc('uid'));
    dump('fetchAllKeyed');
    dump($query->execute()->fetchAllKeyed());

  }

  /**
   * EntityQuery example.
   */
  public function readEntity() {

    $entity_type = 'user';
    // $entity_type = 'node';
    // $entity_type = 'block_content';
    // $entity_type = 'taxonomy_term';
    // $entity_type = 'contact_form';
    // $entity_type = 'file';
    $uids = $this->entityTypeManager()->getStorage($entity_type)->getQuery()
      ->execute();
    $result_users = count($uids);

    $nids = $this->entityTypeManager()->getStorage('node')->getQuery()
      ->condition('type', 'article', '=')
      ->execute();
    $result_article = count($nids);

    $list = [
      'Nombre utilisateur : ' . $result_users,
      'Nombre d\'articles : ' . $result_article,
    ];

    return [
      '#theme' => 'item_list',
      '#items' => $list,
    ];
  }

}
