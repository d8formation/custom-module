<?php

namespace Drupal\custom_module\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;

/**
 * Defines AccessToController class.
 */
class AccessToController extends ControllerBase {

  /**
   * Manage access to controller.
   */
  public function checkUserStatus() {

    $roles = $this->currentUser()->getRoles();
    if (in_array('administrator', $roles)) {
      return AccessResult::allowed();
    }

    $this->messenger()->addWarning($this->t('Please contact the webmaster'));
    return AccessResult::forbidden();
  }

}
