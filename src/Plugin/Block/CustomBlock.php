<?php

namespace Drupal\custom_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class CustomBlock.
 *
 * @Block(
 *  id = "block_simple",
 *  admin_label = @Translation("Block simple"),
 * )
 */
class CustomBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->t('Hello block \o/'),
    ];
  }

}
