<?php

namespace Drupal\custom_module\Plugin\Block\TP;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "tp_block_nv2",
 *  admin_label = @Translation("TP Bloc niveau 2"),
 * )
 */
class TpBlockExo2 extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('form_builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    AccountProxy $current_user,
    FormBuilderInterface $form_builder
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['sport'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose you activity'),
      '#required' => TRUE,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'sport',
        ],
      ],
    ];

    if (isset($config['sport'])) {
      $form['sport']['#default_value'] = $this->entityTypeManager->getStorage('taxonomy_term')->load($config['sport']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['sport'] = $form_state->getValue('sport');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $sport = !empty($config['sport']) ? $config['sport'] : NULL;

    // if (!$this->currentUser->isAnonymous()) {
    //   $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    //   $field_newsletter = $account->get('field_newsletter')->getValue();
    //   if (!empty($field_newsletter)) {
    //     $field_newsletter = reset($field_newsletter);
    //     if ($field_newsletter['value']) {
    //     return;
    //     }
    //   }
    // }

    $form = $this->formBuilder->getForm('Drupal\custom_module\Form\TP\NewsletterSubscribeForm', $sport);
    return $form;
  }

}
