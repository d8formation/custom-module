<?php

namespace Drupal\custom_module\Plugin\Block\TP;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CustomBlock.
 *
 * @Block(
 *  id = "tp_asset",
 *  admin_label = @Translation("TP Asset niveau 2"),
 * )
 */
class TpBlockSlideshow extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['sport'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose you activity'),
      '#required' => TRUE,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'sport',
        ],
      ],
    ];

    if (isset($config['sport'])) {
      $form['sport']['#default_value'] = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->load($config['sport']);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['sport'] = $form_state->getValue('sport');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $sport = !empty($config['sport']) ? $config['sport'] : NULL;

    $slides = [];
    $nids = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'article', '')
      ->condition('field_sport', $sport)
      ->sort('created', 'DESC')
      ->range(0, 3)
      ->execute();

    $entities = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    foreach ($entities as $entity) {
      if ($entity->hasField('field_image')) {
        $field_image = $entity->get('field_image')->getValue();
        if (!empty($field_image)) {
          $field_image = reset($field_image);
          $image_id = $field_image['target_id'];
          $file = $this->entityTypeManager->getStorage('file')->load($image_id);
          $slides[] = [
            'caption' => $entity->getTitle(),
            'url' => file_create_url($file->getFileUri()),
            'alt' => $file->getFileName(),
          ];
        }
      }
    }

    $build = [
      '#theme' => 'block_slideshow',
      '#slides' => $slides,
    ];
    // Manage asset.
    $build['#attached']['library'][] = 'custom_module/rslides';

    return $build;
  }

}
