<?php

namespace Drupal\custom_module\Plugin\Block\TP;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * @Block(
 *  id = "tp_block_nv3",
 *  admin_label = @Translation("TP Bloc niveau 3"),
 * )
 */
class TpBlockExo3 extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactory $config
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $social_network_config = $this->config->get('custom_module.settings');
    return [
      '#theme' => 'block_socialnetwork',
      '#title' => $this->t('Social network'),
      '#config' => $social_network_config->getRawData(),
    ];
  }

}
