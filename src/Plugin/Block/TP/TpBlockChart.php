<?php

namespace Drupal\custom_module\Plugin\Block\TP;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\custom_module\Service\TP\TpChartService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "tp_block_service_chart",
 *  admin_label = @Translation("TP Bloc Chart"),
 * )
 */
class TpBlockChart extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\custom_module\Service\TP\TpChartService
   */
  protected $chartService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
  array $configuration,
    $plugin_id,
  $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('custom_module.chart_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
  $plugin_id,
  $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
  TpChartService $chart_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->chartService = $chart_service;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['sport'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose you activity'),
      '#required' => TRUE,
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'sport',
        ],
      ],
    ];

    if (isset($config['sport'])) {
      $form['sport']['#default_value'] = $this->entityTypeManager->getStorage('taxonomy_term')->load($config['sport']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $sport = $form_state->getValue('sport');
    $this->configuration['sport'] = $sport;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $sport = !empty($config['sport']) ? $config['sport'] : '';

    if ($sport == '') {
      return;
    }

    $sport_term = $this->entityTypeManager->getStorage('taxonomy_term')->load($sport);
    $repartition = $this->chartService->getLevelRepartitionBySport($sport);
    $datas = $this->chartService->preparePieChart($repartition);

    // Manage theme & librairies.
    $build = [
      '#theme' => 'block_chart',
      '#title' => $this->t('Level repartition'),
    ];
    $build['#attached']['library'][] = 'custom_module/highchart';
    $build['#attached']['library'][] = 'custom_module/piechart';
    // Manage data for js file.
    $build['#attached']['drupalSettings']['datas'] = $datas;
    $build['#attached']['drupalSettings']['sport_name'] = $sport_term->getName();

    return $build;
  }

}
