<?php

namespace Drupal\custom_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\custom_module\Service\CustomService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *  id = "custom_bloc_with_service",
 *  admin_label = @Translation("Block custom avec l'utilisation d'un service"),
 * )
 */
class ServiceBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\custom_module\Service\CustomService
   */
  protected $customService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('custom_module.custom_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CustomService $custom_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->customService = $custom_service;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $account_name = $this->customService->getAccountName();
    $foo = $this->customService->doFoo();

    return [
      '#markup' => $account_name . '</br>' . $foo,
    ];
  }

}
