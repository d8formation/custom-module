<?php

/**
 * @file
 * Hook theme file.
 */

 /**
  * Implements hook_theme().
  */
 function custom_module_theme($existing, $type, $theme, $path) {
   $themes['bloc_custom'] = [
       'template' => 'block/block-custom',
       'variables' => [
         'text' => NULL,
         'number' => NULL,
       ],
   ];

   $themes['bloc_slideshow'] = [
     'template' => 'block/block-slideshow',
     'variables' => [
       'slides' => NULL,
     ],
   ];
   return $themes;
 }
