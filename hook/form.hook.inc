<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\node\NodeInterface;

/**
 * @file
 * All hook regarding form.
 */

/**
* Implements hook_form_alter().
*/
function custom_module_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  // You can also use the hook_form_node_article_form_alter()
  if ($form_id == 'node_permanence_form') {

    // $form['title']['#access'] = false;
    $form['title']['widget'][0]['#disabled'] = TRUE;

    // First way : use custom callback in form submit
    // After the valdiation of the form -> need to set a default title
    $form['title']['widget'][0]['value']['#default_value'] = 'default_value';
    array_unshift($form['actions']['submit']['#submit'], '_custom_module_custom_submit');

    // Second way : use the entity form buider
    // Before the validation of the form -> no need to set a default title
    // $form['#entity_builders'][] = '_custom_node_title_builder';
  }
}

 /**
  * Custom validation with form_state
  */
 function _custom_module_custom_submit($form, FormStateInterface &$form_state) {
   $field_adherent = $form_state->getValue('field_adherent');
   $field_adherent = reset($field_adherent);
   $uid = $field_adherent['target_id'];
   $adherent = User::load($uid);
   $custom_title = sprintf('Permanence de %s', $adherent->getAccountName());
   $form_state->setValue('title', [['value' => $custom_title]]);
 }

 /**
  * Custom validation with Entity form builder
  */
 function _custom_node_title_builder($entity_type, NodeInterface $node, $form, FormStateInterface $form_state) {
   $field_adherent = $form_state->getValue('field_adherent');
   $field_adherent = reset($field_adherent);
   $uid = $field_adherent['target_id'];
   $adherent = User::load($uid);
   $custom_title = sprintf('Permanence de %s', $adherent->getAccountName());
   $node->setTitle($custom_title);
 }
