<?php

/**
 * @file
 * All hook regarding entities.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\custom_module\Event\CustomEvent;


/**
 * Implements hook_entity_view_display_alter().
 */
function custom_module_entity_view_display_alter(EntityViewDisplayInterface $display, array $context) {

  if (\Drupal::currentUser()->isAnonymous()) {
    if ($context['bundle'] == 'article' && $context['view_mode'] == 'full') {
      $options = [
        "type" => "text_trimmed",
        "weight" => 0,
        "region" => "content",
        "settings" => [
          "trim_length" => 200
        ],
        "third_party_settings" => [],
        "label" => "hidden",
      ];
      $display->setComponent('body', $options);
    }
  }
}

/**
 * Implements hook_node_update().
 */
function custom_module_node_update(EntityInterface $entity) {
  \Drupal::service('event_dispatcher')->dispatch(CustomEvent::CUSTOM_HANDLER, new CustomEvent($entity));
}
