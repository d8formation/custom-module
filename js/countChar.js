/**
 * @file
 * Contains count character JS function
 */

function countChar(val) {
  var len = val.value.length;
  if (len >= 500) {
    val.value = val.value.substring(0, 500);
  } else {
    jQuery('#countChar').text(500 - len);
  }
};
