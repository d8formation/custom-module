/**
 * @file
 * Contains highchart JS function
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.displayChart = {
    attach: function (context, settings) {
      if (context instanceof HTMLDocument) {
        createChart(drupalSettings);
      }
    }
  };
})(jQuery, Drupal, drupalSettings);

//https://sqndr.github.io/d8-theming-guide/javascript/behaviors.html

function createChart(drupalSettings) {

  var sports = [];
  jQuery.each(drupalSettings.datas[0], function(key, val){
    jQuery.each(val, function(sport, count){
      sports.push(sport);
    })
  });

  var values = [];
  jQuery.each(drupalSettings.datas, function(key, val){
    jQuery.each(val, function(level, details){
      var counts = [];
      jQuery.each(details, function(sport, count){
        counts.push(parseInt(count));
      })
      value = {
        'name': level,
        'data': counts,
      };
      values.push(value);
    })
  });


  Highcharts.chart('container', {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Repartition par sport et par niveau'
      },
      xAxis: {
          categories: sports
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Inscription totale'
          },
          stackLabels: {
              enabled: true,
              style: {
                  fontWeight: 'bold',
                  color: ( // theme
                      Highcharts.defaultOptions.title.style &&
                      Highcharts.defaultOptions.title.style.color
                  ) || 'gray'
              }
          }
      },
      legend: {
          align: 'right',
          x: -30,
          verticalAlign: 'top',
          y: 25,
          floating: true,
          backgroundColor:
              Highcharts.defaultOptions.legend.backgroundColor || 'white',
          borderColor: '#CCC',
          borderWidth: 1,
          shadow: false
      },
      tooltip: {
          headerFormat: '<b>{point.x}</b><br/>',
          pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
      },
      plotOptions: {
          column: {
              stacking: 'normal',
              dataLabels: {
                  enabled: true
              }
          }
      },
      series: values
  });
}
