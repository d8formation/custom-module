/**
 * @file
 * Contains highchart JS function
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.displayChart = {
    attach: function (context, settings) {
      // var scan = $('', ).once('displayChart');
      console.log(context.id);
      if (context.id == 'block-tpblocniveau3') {
        createChart(drupalSettings);
      }
    }
  };
})(jQuery, Drupal, drupalSettings);

function createChart(drupalSettings) {

  var values = [];
  jQuery.each(drupalSettings.datas, function(key, val){
      var value = [];
      value = {
        'name': val.level,
        'y': parseInt(val.count),
      };
      values.push(value);
  });

  Highcharts.chart('container', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
      },
      title: {
          text: 'Répartition des inscrit pour l\'activité ' +  drupalSettings.sport_name
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %'
              }
          }
      },
      series: [{
          name: 'Brands',
          colorByPoint: true,
          data: values
      }]
  });
}
