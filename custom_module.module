<?php

/**
 * @file
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\custom_module\Event\CustomEvent;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;

/**
 * @file
 * All hook are a specific include file.
 */

// $dir = dirname(__FILE__) . '/hook';
// $files = glob($dir . '/*.hook.inc');
// foreach ($files as $hook) {
//   include_once $hook;
// }

/**
 * Implements hook_help().
 */
function custom_module_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.custom_module':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('My module is able to do wonderfull stuff') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('Just install.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_theme().
 */
function custom_module_theme($existing, $type, $theme, $path) {
  $themes['block_custom'] = [
    'template' => 'block/block-custom',
    'variables' => [
      'text' => NULL,
      'number' => NULL,
    ],
  ];

  $themes['block_socialnetwork'] = [
    'template' => 'block/block-socialnetwork',
    'variables' => [
      'title' => NULL,
      'config' => NULL,
    ],
  ];

  $themes['block_slideshow'] = [
    'template' => 'block/block-slideshow',
    'variables' => [
      'slides' => NULL,
    ],
  ];

  $themes['block_chart'] = [
    'template' => 'block/block-chart',
    'variables' => [
      'title' => NULL,
    ],
  ];
  return $themes;
}

//
// CORRECTION DES EXERCICES.
//
//
// TP Controller level 1.
//

/**
 * Implements hook_form_alter().
 */
function custom_module_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  // You can also use the hook_form_node_article_form_alter()
  if ($form_id == 'node_permanence_form') {

    // $form['title']['#access'] = false;
    $form['title']['widget'][0]['#disabled'] = TRUE;

    // First way : use custom callback in form submit
    // After the valdiation of the form -> need to set a default title.
    $form['title']['widget'][0]['value']['#default_value'] = 'default_value';
    array_unshift($form['actions']['submit']['#submit'], '_custom_module_custom_submit');

    // Second way : use the entity form buider
    // Before the validation of the form -> no need to set a default title
    // $form['#entity_builders'][] = '_custom_node_title_builder';.
  }
}

/**
 * Custom validation with form_state.
 */
function _custom_module_custom_submit($form, FormStateInterface &$form_state) {
  $field_adherent = $form_state->getValue('field_adherent');
  $field_adherent = reset($field_adherent);
  $uid = $field_adherent['target_id'];
  $adherent = User::load($uid);
  $custom_title = sprintf('Permanence de %s', $adherent->getAccountName());
  $form_state->setValue('title', [['value' => $custom_title]]);
}

/**
 * Custom validation with Entity form builder.
 */
function _custom_node_title_builder($entity_type, NodeInterface $node, $form, FormStateInterface $form_state) {
  $field_adherent = $form_state->getValue('field_adherent');
  $field_adherent = reset($field_adherent);
  $uid = $field_adherent['target_id'];
  $adherent = User::load($uid);
  $custom_title = sprintf('Permanence de %s', $adherent->getAccountName());
  $node->setTitle($custom_title);
}

//
// TP Controller level 2.
//

/**
 * Implements hook_entity_view_display_alter().
 */
function custom_module_entity_view_display_alter(EntityViewDisplayInterface $display, array $context) {

  if (\Drupal::currentUser()->isAnonymous()) {
    if ($context['bundle'] == 'article' && $context['view_mode'] == 'full') {
      $options = [
        "type" => "text_trimmed",
        "weight" => 0,
        "region" => "content",
        "settings" => [
          "trim_length" => 200,
        ],
        "third_party_settings" => [],
        "label" => "hidden",
      ];
      $display->setComponent('body', $options);
    }
  }
}

//
// TP Controller level 3
// .

/**
 * Implements hook_file_download().
 */
function custom_module_file_download($uri) {

  // $uri = 'private://documentation/index.html';
  if (in_array('documentation', explode('/', $uri))) {
    if (in_array('administrator', \Drupal::currentUser()->getRoles())) {
      return [
        'Content-Type: text/html; charset=utf-8',
      ];
    }
  }
  return -1;
}

/**
 * Implements hook_toolbar().
 */
function custom_module_toolbar() {

  $links = [
    '#theme' => 'item_list',
    '#list-type' => 'ul',
    '#attributes' => [
      'class' => 'toolbar-menu',
    ],
    '#wrapper_attributes' => [
      'class' => 'container',
    ],
    '#items' => [],
  ];

  $links['#items']['documentation'] = [
    '#wrapper_attributes' => [
      'class' => 'menu-item',
    ],
    '#type' => 'link',
    '#title' => t('Documentation'),
    '#url' => Url::fromUri(file_create_url('private://documentation/index.html')),
    '#options' => [
      'attributes' => [
        'title' => t('Documentation'),
        'target' => '_blank',
      ],
    ],
  ];

  $links['#items']['api'] = [
    '#wrapper_attributes' => ['class' => 'menu-item'],
    '#type' => 'link',
    '#title' => t('API Drupal'),
    '#url' => Url::fromUri('https://api.drupal.org/api/drupal'),
    '#options' => [
      'attributes' => [
        'title' => t('Documentation'),
        'target' => '_blank',
      ],
    ],
  ];

  $items['documentation'] = [
    '#type' => 'toolbar_item',
    'tab' => [
      '#type' => 'link',
      '#title' => "Documentation",
      '#url' => Url::fromRoute('<none>'),
      '#options' => [
        'attributes' => [
          'title' => 'Documentation',
          'class' => ['toolbar-icon', 'toolbar-icon-help', 'toolbar-item', 'trigger'],
        ],
      ],
    ],
    'tray' => [
      '#type' => 'container',
      'links' => $links,
    ],
    '#weight' => 500,
  ];

  return $items;
}

//
// TP Event level 3.
//

/**
 * Implements hook_node_update().
 */
function custom_module_node_update(EntityInterface $entity) {
  \Drupal::service('event_dispatcher')->dispatch(CustomEvent::CUSTOM_HANDLER, new CustomEvent($entity));
}
